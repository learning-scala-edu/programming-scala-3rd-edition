package org.elu.scala.learn.shapes

object ProcessMessages:
  def apply(message: Message): Message =
    message match
      case Exit =>
        println("ProcessMessage exiting...")
        Exit
      case Draw(shape) =>
        shape.draw(str => println(s"ProcessMessage: $str"))
        Response(s"ProcessMessage: $shape drawn")
      case Response(unexpected) =>
        val response = Response(s"ERROR: Unexpected Response: $unexpected")
        println(s"ProcessMessage: $response")
        response
